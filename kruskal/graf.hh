#include <string>
#include <fstream>
#include "doubleList.hh"
 
class grafEmptyException{}; //  wyrzucanie bledu
class doNotTouch{}; //  wyrzucanie bledu

template <class Object,class Connect>
class edge; //  inicjalizacja krawedzi

template <class Object,class Connect> //  wierzcholki
class vertex{
private:
  Object wartosc;
  doubleList<edge<Object,Connect>* > edges; //  lista wskazujacych na ten wierzcholek krawedzi
public:
  ~vertex(){edges.clear();}
  edge<Object,Connect>*& operator[](int i){  // zwracanie wskazanej krawedzie z listy
    if(i>=edges.rozmiar())throw doNotTouch(); //  wskazanie po za liste
    return edges[i];
  }
  Object& getZawartosc(){return wartosc;}
  doubleList<edge<Object,Connect>* >& getList(){return edges;} //  zwraca dostep do listy
  bool operator!=(vertex<Object,Connect>& a){if(this!=&a)return true;return false;}
  bool operator==(vertex<Object,Connect>& a){if(wartosc==a.getZawartosc())return true;return false;}
};

template <class Object,class Connect> //  wyswietlanie wierzcholkow
std::ostream& operator << (std::ostream& StrmWy,vertex<Object,Connect> &WyswietlanaWartosc){
  std::cout<<WyswietlanaWartosc.getZawartosc()<<" ";
  return StrmWy;  
}

template <class Object,class Connect> //  krawedzie
class edge{
private:
  Connect wartosc;
  bool wyswietl; //  czy wartosc krawedzi ma byc wyswietlona
  bool exist;
  vertex<Object,Connect>* v1; //  pierwszy wskazywany element
  vertex<Object,Connect>* v2; //  drugi wskazywany element
public:
  edge(){wyswietl=false;exist=false;}
  bool& getWyswietl(){return wyswietl;}
  bool& getExist(){return exist;}
  Connect& getZawartosc(){return wartosc;}
  vertex<Object,Connect>*& getV1(){return v1;}
  vertex<Object,Connect>*& getV2(){return v2;}
  bool operator!=(edge<Object,Connect>& a){if(this!=&a)return true;return false;}
  bool operator==(edge<Object,Connect>& a){if(this==&a)return true;return false;}
};

template <class Object,class Connect> //  wyswietlanie krawedzi
std::ostream& operator << (std::ostream& StrmWy,edge<Object,Connect> &WyswietlanaWartosc){
  if(WyswietlanaWartosc.getWyswietl())std::cout<<std::endl<<*WyswietlanaWartosc.getV1()<<" << "<<WyswietlanaWartosc.getZawartosc()<<" >> "<<*WyswietlanaWartosc.getV2();
  else std::cout<<std::endl<<*WyswietlanaWartosc.getV1()<<" >>>> "<<*WyswietlanaWartosc.getV2();
  return StrmWy;  
}

template <class Object,class Connect> //  klasa grafu
class graf{
protected:
  doubleList<vertex<Object,Connect> >V; //  lista wierzcholkow
  doubleList<edge<Object,Connect> >E; //  lista krawedzi
  int dlMatrix;
  edge<Object,Connect> **matrix;
  int typGrafu;
  void clear(edge<Object,Connect> **tablica,int dl);
  void przepisz(edge<Object,Connect> **newtablica,edge<Object,Connect> **tablica,int dl);
public:
  graf(){typGrafu=0;dlMatrix=0;}
  graf(int a){typGrafu=a;dlMatrix=0;}
  ~graf(){if(typGrafu){}else{V.clear();E.clear();}}
  int findint(vertex<Object,Connect>& v);
  int getDlMatrix(){return dlMatrix;}
  int**& getMatrix(){return matrix;}
  int getTypGrafu(){return typGrafu;}
  void clear(); //  czyszczenie listy krawedzi i listy wierzcholkow
  void save(const char* nazwa);
  void load(const char* nazwa);
  void generate(const int iloscV,const int d);
  void generateToFile(const int iloscV,const int d,const char* nazwa);
  vertex<Object,Connect>& findVertex(Object dane)throw(grafEmptyException); //  znajdowanie adresu wierzcholka na podstawie zawartosci
  edge<Object,Connect>& findEdge(Connect dane)throw(grafEmptyException); //  znajdowanie adresu krawedzi na podstawie zawartosci
  Object& insertVertex(Object dane); //  dodawanie wierzcholka
  Object& removeVertex(vertex<Object,Connect>& a)throw(grafEmptyException); //  usuwanie wierzcholka
  void insertEdge(vertex<Object,Connect>& a,vertex<Object,Connect>& b);  // dodanie krawedzi
  Connect insertEdge(vertex<Object,Connect>& a,vertex<Object,Connect>& b,Connect dana); // dodanie krawedzi
  Connect removeEdge(edge<Object,Connect>& a)throw(grafEmptyException);  // usuwanie krawedzi
  int edges(); //  wyswietlenie polaczen
  int vertices(); //  wyswietlenie elementow
  int incidentEdges(vertex<Object,Connect>& v); //  wyswietlenie krawedzi przyleglych do wskazanego wierzcholka
  vertex<Object,Connect>& endVerticies(edge<Object,Connect>& e,int i)throw(grafEmptyException);  //wierzcholki na koncach wskazanej krawedzi
  vertex<Object,Connect>& opposite(vertex<Object,Connect>& v,edge<Object,Connect>& e)throw(grafEmptyException); //  wierzcholek po przeciwnej stronie wzgledem wskazanej krawedzi i wierzcholka
  bool areAdjacent(vertex<Object,Connect>& v1,vertex<Object,Connect>& v2); //  czy sa polaczone
  void replace(vertex<Object,Connect>& v,Object x); //  zamien zawartosc wskazanego wierzcholka
  void replace(edge<Object,Connect>& e,Connect x); //  zamien zawartosc wskazanej krawedzi
  vertex<Object,Connect>& operator[](int i){return V[i];} //  zwroc wskazany element z listy wierzcholkow
  edge<Object,Connect>& operator()(int i){return E[i];} //  zwroc wskazany element z listy krawedzi
  edge<Object,Connect>& operator()(int i,int j){return matrix[i][j];} //  zwroc wskazany element z listy krawedzi
  doubleList<vertex<Object,Connect> >& getListV(){return V;} //  zwraca dostep do listy
  doubleList<edge<Object,Connect> >& getListE(){return E;} //  zwraca dostep do listy
};

template <class Object,class Connect>
void graf<Object,Connect>::clear(edge<Object,Connect> **tablica,int dl){
  for(int i=0;i<dl;i++){
    delete [] tablica[i];
  }
  delete [] tablica; // zwalnianie pamieci z tablicy wskaznikow odpowiadajacej kolumna
  tablica=NULL;
}

template <class Object,class Connect>
void graf<Object,Connect>::przepisz(edge<Object,Connect> **newtablica,edge<Object,Connect> **tablica,int dl){
  for(int i=0;i<dl;i++){
    for(int j=0;j<dl;j++){
      newtablica[i][j]=tablica[i][j];
    }
  }
  clear(tablica,dl);
  tablica=newtablica;
}

template<class Object,class Connect>
void graf<Object,Connect>::clear(){
  if(typGrafu){
    clear(matrix,dlMatrix);
    dlMatrix=0;
  }else{
    E.clear(); //  czyszczenie listy krawedzi
  }
  V.clear(); //  czyszczenie listy wierzcholkow
}

template<class Object,class Connect>
int graf<Object,Connect>::findint(vertex<Object,Connect>& v){
  int i=0;
  while(1){
    if(v==V[i])return i;
    i++;
    if(i==V.rozmiar())return -1;
  }
}

template<class Object,class Connect>
vertex<Object,Connect>& graf<Object,Connect>::findVertex(Object dane)throw(grafEmptyException){
  for(int i=0;i<V.rozmiar();i++){
    if(V[i].getZawartosc()==dane)return V[i]; //  zwraca wierzcholek o wskazanej wartosci
    }
  throw grafEmptyException(); //  jezeli nie ma takiego wierzcholku to zwroc blad
}

template<class Object,class Connect>
edge<Object,Connect>& graf<Object,Connect>::findEdge(Connect dane)throw(grafEmptyException){
  if(typGrafu){
    for(int i=0;i<dlMatrix;i++){
      for(int j=0;j<dlMatrix;j++){
	if(matrix[i][j].getZawartosc()==dane)return matrix[i][j]; //  zwraca krawedz o wskazanej wartosci
      }
    }
    throw grafEmptyException(); //  jeżeli nie ma takiej krawedzi o wskazanej wartosci
  }else{
    for(int i=0;i<E.rozmiar();i++){
      if(E[i].getZawartosc()==dane)return E[i]; //  zwraca krawedz o wskazanej wartosci
    }
    throw grafEmptyException(); //  jeżeli nie ma takiej krawedzi o wskazanej wartosci
  }
}

template<class Object,class Connect>
Object& graf<Object,Connect>::insertVertex(Object dane){
  vertex<Object,Connect> tmp;
  edge<Object,Connect> **tablica;  
  if(typGrafu){
    dlMatrix++;
    tablica=new edge<Object,Connect>* [dlMatrix]; // stworzenie tablicy wskaznikow o dlugosci wiersza tablicy
    for(int i=0;i<dlMatrix;i++){
      tablica[i]=new edge<Object,Connect> [dlMatrix]; // tworzenie kolumn z tablicy wskaznikow
    }
    if(dlMatrix>1){
      przepisz(tablica,matrix,dlMatrix-1);
      matrix=tablica;
    }else matrix=tablica;
  }
  tmp.getZawartosc()=dane;
  V.wstawOstatni(tmp); //  dodanie elementu do listy
  return tmp.getZawartosc();
}

template<class Object,class Connect>
Object& graf<Object,Connect>::removeVertex(vertex<Object,Connect>& a)throw(grafEmptyException){
  edge<Object,Connect> **newtablica;
  int wiersz;
  if(typGrafu){
    wiersz=findint(a);
    newtablica=new edge<Object,Connect>* [dlMatrix-1]; // stworzenie tablicy wskaznikow o dlugosci wiersza tablicy
    for(int i=0;i<(dlMatrix-1);i++){
      newtablica[i]=new edge<Object,Connect> [dlMatrix-1]; // tworzenie kolumn z tablicy wskaznikow
    }
    for(int i=0;i<(wiersz);i++){
      for(int j=0;j<(wiersz);j++){
	newtablica[j][i]=matrix[j][i]; 
      } 
    }
    for(int i=wiersz;i<(dlMatrix-1);i++){
      for(int j=wiersz;j<(dlMatrix-1);j++){
	newtablica[j][i]=matrix[j+1][i+1]; 
      } 
    }
    for(int i=wiersz;i<(dlMatrix-1);i++){
      for(int j=0;j<(wiersz);j++){
	newtablica[j][i]=matrix[j][i+1]; 
      } 
    }
    for(int i=0;i<(wiersz);i++){
      for(int j=wiersz;j<(dlMatrix-1);j++){
	newtablica[j][i]=matrix[j+1][i]; 
      } 
    }
    clear(matrix,dlMatrix);
    dlMatrix--;
    matrix=newtablica;
    newtablica=NULL;
    try{
      return V.usunWskazany(a).getZawartosc(); //  zwraca zawartosc usuwanego wierzcholka
    }catch(doubleListEmptyException){
      throw grafEmptyException(); //  jezeli wskazany wierzcholek nie istenieje
    }
  }else{
    try{
      while(!a.getList().isEmpty()){E.usunWskazany(*a.getList().usunPierwszy());}
      return V.usunWskazany(a).getZawartosc(); //  zwraca zawartosc usuwanego wierzcholka
    }catch(doubleListEmptyException){
      throw grafEmptyException(); //  jezeli wskazany wierzcholek nie istenieje
    }
  }
}

template<class Object,class Connect>
void graf<Object,Connect>::insertEdge(vertex<Object,Connect>& a,vertex<Object,Connect>& b){ //  zawartosc dodanej krawedzi to 0
  edge<Object,Connect> tmp;
  if(typGrafu){
    if(!this->areAdjacent(a,b)){
      matrix[this->findint(b)][this->findint(a)].getWyswietl()=false;
      matrix[this->findint(b)][this->findint(a)].getExist()=true;
      matrix[this->findint(b)][this->findint(a)].getV1()=&a;
      matrix[this->findint(b)][this->findint(a)].getV2()=&b;
    }
  }else{
    if(!this->areAdjacent(a,b)){
      tmp.getZawartosc()=0;
      tmp.getV1()=&a;
      tmp.getV2()=&b;
      E.wstawOstatni(tmp); //  dodanie krawedzi do listy
      a.getList().wstawOstatni(&E.ostatni()); //  przypisanie w wierzcholku dodanej krawedzi
      b.getList().wstawOstatni(&E.ostatni()); //  przypisanie w wierzcholku dodanej krawedzi
    }
  }
}

template<class Object,class Connect>
Connect graf<Object,Connect>::removeEdge(edge<Object,Connect>& a)throw(grafEmptyException){
  edge<Object,Connect>* tmp=&a;
  if(typGrafu){
    for(int i=0;i<dlMatrix;i++){
      for(int j=0;j<dlMatrix;j++){
	if(matrix[i][j]==a){
	  matrix[i][j].getWyswietl()=false;
	  matrix[i][j].getExist()=false;
	  matrix[i][j].getV1()=NULL;
	  matrix[i][j].getV2()=NULL;    
	  return matrix[i][j].getZawartosc(); //  zwraca wartosc usuwanej krawedzi
	}
      }
    }
    throw grafEmptyException(); //  gdy wskazana krawedz nie istnieje    
  }else{
    try{
      a.getV1()->getList().usunWskazany(tmp);
      a.getV2()->getList().usunWskazany(tmp);
      return E.usunWskazany(a).getZawartosc(); //  zwraca wartosc usuwanej krawedzi
    }catch(doubleListEmptyException){
      throw grafEmptyException(); //  gdy wskazana krawedz nie istnieje
    }
  }
}

template<class Object,class Connect>
Connect graf<Object,Connect>::insertEdge(vertex<Object,Connect>& a,vertex<Object,Connect>& b,Connect dana){ //  dodawanie krawedzi ale z podana jej wartoscia
  edge<Object,Connect> tmp;
  int inda,indb;
  if(typGrafu){
    inda=this->findint(a);
    indb=this->findint(b);
    if(!this->areAdjacent(a,b)){
      matrix[indb][inda].getWyswietl()=true;
      matrix[indb][inda].getZawartosc()=dana;
      matrix[indb][inda].getExist()=true;
      matrix[indb][inda].getV1()=&a;
      matrix[indb][inda].getV2()=&b;
      matrix[inda][indb].getWyswietl()=true;
      matrix[inda][indb].getZawartosc()=dana;
      matrix[inda][indb].getExist()=true;
      matrix[inda][indb].getV1()=&a;
      matrix[inda][indb].getV2()=&b;
    }
    return matrix[indb][inda].getZawartosc();
  }else{
    if(!this->areAdjacent(a,b)){
      tmp.getZawartosc()=dana;
      tmp.getV1()=&a;
      tmp.getV2()=&b;
      tmp.getWyswietl()=true;
      E.wstawOstatni(tmp);
      a.getList().wstawOstatni(&E.ostatni());
      b.getList().wstawOstatni(&E.ostatni());
    }
    return tmp.getZawartosc();
  }
}

template<class Object,class Connect>
int graf<Object,Connect>::edges(){
  int liczba=0;
  if(typGrafu){
    for(int j=0;j<dlMatrix;j++){
      for(int i=0;i<j;i++){
        if(matrix[i][j].getExist()){
	  std::cout<<V[j]<<"<< "<<matrix[i][j].getZawartosc()<<" >> "<<V[i]<<std::endl;
	  liczba++;
	}
      }
    }
    return liczba;
  }else{
    if(E.isEmpty())return 0; //  gdy pusta to zwraca 0
    E.screen(); //  wyswietlenie listy krawedzi
    std::cout<<std::endl; // zwraca rozmiar listy
    return E.rozmiar();
  }
}

template<class Object,class Connect>
int graf<Object,Connect>::vertices(){
  if(V.isEmpty())return 0; //  gdy pusta to zwraca 0
  V.screen(); //  wyswietlenie listy wierzcholkow
  return V.rozmiar(); // zwraca rozmiar listy
}

template<class Object,class Connect>
int graf<Object,Connect>::incidentEdges(vertex<Object,Connect>& v){
  int liczba=0;
  int wartosc=0;
  if(typGrafu){
    wartosc=this->findint(v);
    for(int i=0;i<dlMatrix;i++){
      if(matrix[i][wartosc].getExist()){
	std::cout<<std::endl<<V[wartosc]<<">> "<<matrix[i][wartosc].getZawartosc()<<" >> "<<V[i];
	liczba++;
      }
    }
    std::cout<<std::endl;
    return liczba; //  zwraca rozmiar wyswietlonej listy
  }else{
    for(int i=0;i<v.getList().rozmiar();i++){
      std::cout<<*v[i]<<" "; //  wyswietla krawedzie przylegle
    }
    std::cout<<std::endl;
    return v.getList().rozmiar(); //  zwraca rozmiar wyswietlonej listy
  }
}

template<class Object,class Connect>
vertex<Object,Connect>& graf<Object,Connect>::endVerticies(edge<Object,Connect>& e,int i)throw(grafEmptyException){
  if(typGrafu){
    if(e.getExist()){
      if(i)return *e.getV2(); //  zwraca adres jednego wierzcholka laczonego przez wskazana krawedz
      return *e.getV1(); //  zwraca adres jednego wierzcholka laczonego przez wskazana krawedz
    }else throw grafEmptyException();
  }else{
    if(i)return *e.getV2(); //  zwraca adres jednego wierzcholka laczonego przez wskazana krawedz
    return *e.getV1(); //  zwraca adres jednego wierzcholka laczonego przez wskazana krawedz
  }
}

template<class Object,class Connect>
vertex<Object,Connect>& graf<Object,Connect>::opposite(vertex<Object,Connect>& v,edge<Object,Connect>& e)throw(grafEmptyException){
  if(e.getV1()==&v)return *e.getV2(); //  jezeli V1 to jest podany wierzcholek to zwroc V2
  if(e.getV2()==&v)return *e.getV1(); //  jezeli V1 to jest podany wierzcholek to zwroc V2
  throw grafEmptyException();
}

template<class Object,class Connect>
bool graf<Object,Connect>::areAdjacent(vertex<Object,Connect>& v1,vertex<Object,Connect>& v2){
  if(typGrafu){
    return matrix[findint(v2)][findint(v1)].getExist();
  }else{
    for(int i=0;i<v1.getList().rozmiar();i++){
      if((v1[i]->getV1()==&v2)||(v1[i]->getV2()==&v2))return true; //  zwroc prawde jezeli ktoras krawedz wierzcholka V1 wskazuje V2
      //if(v1[i]->getV2()==&v2)return true; //  zwroc prawde jezeli ktoras krawedz wierzcholka V1 wskazuje V2
    }
    return false;
  }
}

template<class Object,class Connect>
void graf<Object,Connect>::replace(vertex<Object,Connect>& v,Object x){
  v.getZawartosc()=x; //  podmiana wartosci
}

template<class Object,class Connect>
void graf<Object,Connect>::replace(edge<Object,Connect>& e,Connect x){
  e.getZawartosc()=x; //  podmiana wartosci
}

template<class Object,class Connect>
void graf<Object,Connect>::save(const char* nazwa){ // zapisuje graf do pliku txt
  std::ofstream plik(nazwa); // otwierza plik o nazwie z parametru wywolania metory
  plik<<V.rozmiar()<<std::endl; // zapsuje ilosc wierzcholkow
  for(int i=0;i<V.rozmiar();i++){ // zapisuje wszystkie wierzcholki od nowej lini
    plik<<V[i].getZawartosc()<<std::endl;
  }
  if(typGrafu==0){ // dla grafu na liscie sasiedztwa 
    for(int i=0;i<E.rozmiar();i++){ // w nowych liniach wypisuje wszystkie krawedzie w formacie dana_wierzcholka dana_krawedzi dana_wierzcholka
      plik<<this->findint(*E[i].getV1())<<" "<<E[i].getZawartosc()<<" "<<this->findint(*E[i].getV2())<<std::endl;
    }
  }else{ // dla grafu na macierzy
    for(int i=0;i<dlMatrix;i++){
      for(int j=0;j<dlMatrix;j++){ // dla wszystkich mozliwych krawedzi
	if(matrix[i][j].getExist())plik<<i<<" "<<matrix[i][j].getZawartosc()<<" "<<j<<std::endl; // wypisuje krawedzie w ten sam sposob co przy liscie sasiedztwa ale tylko dla krawedzi istniejacych 
      }
    }   
  }
  plik.close(); // zamyka plik
}

template<class Object,class Connect>
void graf<Object,Connect>::load(const char* nazwa){ // wczytuje graf z pliku txt niezaleznie czy jest na macierzy sasiedztwa czy na liscie
  std::ifstream plik(nazwa); // otwierza plik o nazwie z parametru wywolania metory
  Object obj; // pomocnicza zmienna zawartosci wierzcholka
  Connect con; // pomocnicza zmienna zawartosci krawedzi
  int rozmiarV,tmp,tmp2; // pomocnicze zmienne rozmiaru i indeksow
  plik>>rozmiarV; // wczytuje rozmiar
  // poniewarz plik jest identyczny dla grafu na liscie sasiedztwa i macierzy sasiedztwa to nie trzeba rozbijac na przypadki
  for(int i=0;i<rozmiarV;i++){ // dodaje wszystkie wierzcholki
    plik>>obj;
    this->insertVertex(obj);
  }
  while(plik>>tmp){ // dodaje wszystkie krawedzie
    plik>>con; // tmp trzyma indeks jednego wierzcholka, con wartosc krawedzi, a tmp2 indeks drugiego wierzcholka
    plik>>tmp2;
    this->insertEdge(V[tmp],V[tmp2],con); // dodaje krawedzie
  }
  plik.close(); // zamyka plik
}

template<class Object,class Connect>
void graf<Object,Connect>::generateToFile(const int iloscV,const int d,const char* nazwa){ // generowanie grafu w pliku txt identycznym jak te z funkcji save()
  std::ofstream plik(nazwa); // otwiera plik o nazwie z parametru wywolania
  bool ind[iloscV][iloscV]; // zmienna pomocnicza indeksyjaca
  int tmp1,tmp2; // zmienne pomocnicze
  Connect con; // zmienna pomocnicza wartosci krawedzi
  int iloscE=(d*iloscV*(iloscV-1))/200; // ilosc generowanych krawedzi
  int zakres=10*iloscE; // okresla z jakiego zakresu moga byc losowane wartosci krawedzi
  srand(time(NULL)); // ustawienie zegara
  plik<<iloscV<<std::endl; // zapisuje rozmiar
  for(int i=0;i<iloscV;i++){ // w nowych liniach zapisuje wierzcholki
    this->insertVertex(i);
    plik<<i<<std::endl;
  }
  for(int i=0;i<iloscV;i++){
    for(int j=0;j<iloscV;j++){ // tworzy macierz trujkatna gdzie tylko powyzej diagonalnej jest zaznaczone ze polaczenia jeszcze nie istnieja
      if(i<j)ind[i][j]=0;
      else ind[i][j]=1;
    }
  }
  while(iloscE){ // dla wszystkich krawedzi
    con=rand()%zakres; // losuje wartosc krawedzi
    tmp1=rand()%iloscV; // losuje wierzcholki
    tmp2=rand()%iloscV;
    while(ind[tmp2][tmp1]){ // sprawdza czy to polaczenie juz istnieje i szuka az znajdzie nieistniejace
      tmp1++; // jezeli tak to zweksza wartosc tmp1
      if(tmp1==iloscV){ // jezeli tmp1 przekroczy zakres
	tmp2++; // zwieksza wartosc tmp2
        tmp1=0; // tmp1 leci od poczatku wiersza
      }
      if(tmp2==iloscV)tmp2=0; // jezeli tmp2 przekroczy zakres to leci od oczatku kolumny
    }
    ind[tmp2][tmp1]=1; // zaznacza ze juz istnieje to polaczenie
    plik<<tmp1<<" "<<con<<" "<<tmp2<<std::endl; // dodaje to polaczenie do pliku
    iloscE--; // zmiejsza ilosc polaczen do wygenerowania
  } 
  plik.close(); // zamyka plik
}


template<class Object,class Connect>
void graf<Object,Connect>::generate(const int iloscV,const int d){ // generuje graf
  vertex<Object,Connect>* vert[iloscV]; // pomocnicza tablica wskaznikow na wierzcholki
  int tmp1,tmp2; // pomocnicze tablice indeksujace
  bool end=true; // pomocnicza zmienna do petli
  Connect con; // pomocnicza zmienna trzymajaca wartosc krawedzi
  int iloscKrawedzi[iloscV]; // okresla ile krawedzi ma wybrany wierzcholek
  int iloscE=(d*iloscV*(iloscV-1))/200; // ilosc wszystkich krawedzi
  int zakres=10*iloscE; // zakres z jakiego losowane beda wartosci krawedzi
  srand(time(NULL)); // ustawienie zegara
  for(int i=0;i<iloscV;i++){ // dodanie wszystkich krawedzi
    this->insertVertex(i);
    vert[i]=&V.ostatni(); // przypisanie wskaznikow
    iloscKrawedzi[i]=0; // przypisanie ilosci krawedzi dla wierzcholkow
  }
  while(iloscE){ // dla wszystkich krawedzi grafu
    con=rand()%zakres; // losuje wartosc krawedzi
    tmp1=rand()%iloscV; // losuje wierzcholki
    tmp2=rand()%iloscV;
    if(typGrafu==0){ // dla grafu na liscie sasiedztwa
      while(end){
	if(tmp1==tmp2){ // jezeli wylosowano dwa razy ten sam wierzcholek
	  tmp1++; // zmien jeden z wierzcholkow
	  if(tmp1==iloscV)tmp1=0; // jezeli indeks wyszedl po za zakres to rowny jest 0
	}else if(this->areAdjacent(*vert[tmp1],*vert[tmp2])){ // jezeli krawedzie juz sa polaczone
	  if(vert[tmp2]->getList().rozmiar()==(iloscV-1)){ // jezeli jeden wierzcholek juz ma wszystke mozliwe krawedzie
	    tmp2++; // zmieniamy ten wierzcholek
	    if(tmp1==tmp2)tmp2++; // jezeli zmienilismy na ten sam wierzcholek co drugi to idziemy na nastepny
	    if(tmp2==iloscV)tmp2=0; // jesli wyszlismy po za zakres to idziemy na pierwszy
	  }else{ // gdy wierzcholek nie ma jeszcze wszystkich mozliwych krawedzi to zmieniamy drugi wierzcholek by w tym utworzyc
	    tmp1++; // analogicznie jak w wczesniejszym warunku
	    if(tmp1==tmp2)tmp1++;
	    if(tmp1==iloscV)tmp1=0;
	  }
	}else{ // gdy mamy dwa rozne i niepolaczone wierzcholki to dodajemy krawedz miedzy nimi
	  this->insertEdge(*vert[tmp1],*vert[tmp2],con);
	  end=false; // konczymy petle while
	}
      }
    }else{ // dla grafu na macierzy sasiedztwa
      while(end){ // analogicznie jak dla grqafu na liscie sasiedztwa
	if(tmp1==tmp2){
	  tmp1++;
	  if(tmp1==iloscV)tmp1=0;
	}else if(this->areAdjacent(*vert[tmp1],*vert[tmp2])){
	  if(iloscKrawedzi[tmp2]==(iloscV-1)){
	    tmp2++;
	    if(tmp1==tmp2)tmp2++;
	    if(tmp2==iloscV)tmp2=0;
	  }else{
	    tmp1++;
	    if(tmp1==tmp2)tmp1++;
	    if(tmp1==iloscV)tmp1=0;
	  }
	}else{
	  this->insertEdge(*vert[tmp1],*vert[tmp2],con);
	  end=false;
	  iloscKrawedzi[tmp1]++;
	  iloscKrawedzi[tmp2]++;
	}
      }      
    }
    end=true; // zmieniamy end na true by mozna bylo znow wejsc do while przy nastepnej krawedzi
    iloscE--; // zmniejszamy ilosc krawedzi do dodania
  } 
}
