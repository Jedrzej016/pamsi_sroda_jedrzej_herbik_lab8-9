#include<iostream>
#include <string>
#include <cstdlib>
#include <ctime>
#include "graf.hh"
#include "kolejka.hh"

using namespace std;

int dl_kolejki=0;

template <class Object,class Connect>
int Kruskala(graf<Object,Connect>& poddrzewo,graf<Object,Connect>& graf);

int main(int argc, char *argv[]){
  graf<int,int> grafm(1);
  graf<int,int> graf2m(1);
  graf<int,int> graf2;
  graf<int,int> graf;
  graf.generate(10,100);
  cout<<endl<<"Krawedzie grafu Kluski";
  graf.edges();
  cout<<endl<<"Wierzcholki grafu Kluski"<<endl;
  graf.vertices();
  grafm.generate(10,100);
  cout<<endl<<endl<<"Krawedzie grafu na macierzy";
  graf.edges();
  cout<<endl<<"Wierzcholki grafu na macierzy"<<endl;
  graf.vertices();
  Kruskala(graf2,graf);
  Kruskala(graf2m,grafm);
  graf.clear();
  grafm.clear();
  cout<<endl<<endl<<"Krawedzie podgrafu Kluski";
  graf2.edges();
  cout<<endl<<"Wierzcholki podgrafu Kluski"<<endl;
  graf2.vertices();
  cout<<endl<<endl<<"Krawedzie podgrafu na macierzy"<<endl;
  graf2m.edges();
  cout<<endl<<"Wierzcholki podgrafu na macierzy"<<endl;
  graf2m.vertices();
  graf2.clear();
  graf2m.clear();
  cout<<endl<<endl;
}

template <class Object,class Connect>
int Kruskala(graf<Object,Connect>& poddrzewo,graf<Object,Connect>& graf){
  int iloscE=graf.getListE().rozmiar(); // okresla ilosc krawedzi w grafie
  int iloscV=graf.getListV().rozmiar(); // okresla ilosc wierzcholkow w grafie
  vertex<Object,Connect> **vert=new vertex<Object,Connect>* [iloscV]; // tablica wskaznikow na wierzcholki grafu
  vertex<Object,Connect> **vertPod=new vertex<Object,Connect>* [iloscV]; // tablica wskaznikow na wierzcholki drzewa rozpinajacego
  int *klaster=new int[iloscV]; // tablica okreslajaca czy wierzcholki sa polaczone
  bool *istnieje=new bool[iloscV]; // tablica okreslajaca czy wybrane wierzcholki już zostaly doadane do poddrzewa
  edge<Object,Connect>* Ed=NULL; // pomocniczy wskaznik na krawedz
  KolejkaPrio<edge<Object,Connect>* > Prio; // kolejka priorytetowa
  int V1,V2; //pomocnicze zmienne
  if(graf.getListV().rozmiar()==0) return 0; // jezeli nie ma wierzcholkow to zakoncz
  else if(graf.getListV().rozmiar()==1){ // jezeli jest tylko jeden wierzcholek to min drzewo rozpinajace to on
    poddrzewo.insertVertex(graf[1].getZawartosc());
    return 0;
  }
  for(int i=0;i<iloscV;i++){ // dodanie adresow na wierzcholki i rzygotowanie tablic
    vert[i]=&graf[i];
    klaster[i]=i;
    istnieje[i]=false;
  }
  if(graf.getTypGrafu()==0){ // jezeli graf na liscie sasiedztwa
    for(int i=0;i<iloscE;i++){ // dodanie adresow krawedzie do tablicy
      Ed=&graf(i);
      Prio.dodaj(Ed,Ed->getZawartosc()); // wsadzenie wszystkich krawedzi do kolejki priorytetowej
    }
    while(!Prio.isEmpty()){ // dopoki kolejka nie jest pusta
      Ed=Prio.usunMin().getZawartosc(); // dodanie adresu zdjetej krawedzi to pomocniczego wskaznika
      V1=graf.findint(*Ed->getV1()); // podanie indeksu jednego wierzcholka krawedzi
      V2=graf.findint(*Ed->getV2()); // podanie indeksu drugiego wierzcholka krawedzi
      if(klaster[V1]!=klaster[V2]){ // jezeli jeszcze nie sa polaczone
	if(!istnieje[V1]){ // jezeli jeszcze nie ma tego wierzcholka w drzewie rozpinajacym
	  istnieje[V1]=true; // zaznaccza ze juz jest w drzewie
	  poddrzewo.insertVertex(vert[V1]->getZawartosc()); // dodaje do drzewa
	  vertPod[V1]=&poddrzewo.getListV().ostatni(); // dodaje adres wierzcholka do tablicy
	}
	if(!istnieje[V2]){ // to samo dla drugiego wiercholka krawedzi
	  istnieje[V2]=true;
	  poddrzewo.insertVertex(vert[V2]->getZawartosc());
	  vertPod[V2]=&poddrzewo.getListV().ostatni();
	}
	poddrzewo.insertEdge(*vertPod[V1],*vertPod[V2],Ed->getZawartosc()); // laczy wybrane wierzcholki
	for(int i=0,j=klaster[V2];i<iloscV;i++)if(klaster[i]==j)klaster[i]=klaster[V1]; // wszystkim polaczonym wierzcholka nadaje jeden klaster
      }
    }
  }else if(graf.getTypGrafu()==1){ // dla grafu na macierzy sasiedztwa
    for(int i=0;i<iloscV;i++){
      for(int j=0;j<i;j++){ // dla wszystkich mozliwych wierzcholkow
	Ed=&graf(i,j); // pomocniczy wskaznik na wybrana krawedz
	if(Ed->getExist()){ // rozpatrz tylko istniejace
	  Prio.dodaj(Ed,Ed->getZawartosc()); // dodaj do kolejki priorytetowej
	}
      }
    }
    while(!Prio.isEmpty()){ // analogicznie jak dla listy sasiedztwa
      Ed=Prio.usunMin().getZawartosc();
      V1=graf.findint(graf.endVerticies(*Ed,0));
      V2=graf.findint(graf.endVerticies(*Ed,1));
      if(klaster[V1]!=klaster[V2]){
	if(!istnieje[V1]){
	  istnieje[V1]=true;
	  poddrzewo.insertVertex(vert[V1]->getZawartosc());
	  vertPod[V1]=&poddrzewo.getListV().ostatni();
	}
	if(!istnieje[V2]){
	  istnieje[V2]=true;
	  poddrzewo.insertVertex(vert[V2]->getZawartosc());
	  vertPod[V2]=&poddrzewo.getListV().ostatni();
	}
	poddrzewo.insertEdge(*vertPod[V1],*vertPod[V2],Ed->getZawartosc());
	for(int i=0,j=klaster[V2];i<iloscV;i++)if(klaster[i]==j)klaster[i]=klaster[V1];
      }
    }
  }
  Prio.clear(); // czyszczenie kolejki dla pewnosci
  delete[](vert); // czyszczenie wszystkich pomocniczych tablic
  delete[](vertPod);
  delete[](klaster);
  delete[](istnieje);
  return 1;
}
