#include<iostream>
#include <string>
#include <cstdlib>
#include <ctime>
#include "graf.hh"
#include "kolejka.hh"

using namespace std;

int dl_kolejki=0;

template <class Object,class Connect>
int Prima(graf<Object,Connect>& poddrzewo,graf<Object,Connect>& graf);

int main(int argc, char *argv[]){
  graf<int,int> grafm(1);
  graf<int,int> graf2m(1);
  graf<int,int> graf2;
  graf<int,int> graf;
  graf.generate(10,100);
  cout<<endl<<"Krawedzie grafu";
  graf.edges();
  cout<<endl<<"Wierzcholki grafu"<<endl;
  graf.vertices();
  grafm.generate(10,100);
  cout<<endl<<"Krawedzie grafu na macierzy";
  graf.edges();
  cout<<endl<<"Wierzcholki grafu na macierzy"<<endl;
  graf.vertices();
  Prima(graf2,graf);
  Prima(graf2m,grafm);
  graf.clear();
  grafm.clear();
  cout<<endl<<endl<<"Krawedzie podgrafu";
  graf2.edges();
  cout<<endl<<"Wierzcholki podgrafu"<<endl;
  graf2.vertices();
  cout<<endl<<endl<<"Krawedzie podgrafu na macierzy"<<endl;
  graf2m.edges();
  cout<<endl<<"Wierzcholki podgrafu na macierzy"<<endl;
  graf2m.vertices();
  graf2.clear();
  graf2m.clear();
  cout<<endl<<endl;
}

template <class Object,class Connect>
int Prima(graf<Object,Connect>& poddrzewo,graf<Object,Connect>& graf){
  int ilosc=graf.getListV().rozmiar(); // okresla ilosc wierzcholkow
  int *D=NULL; // do okreslenia odleglosci wierzcholkow od polaczonej struktory
  vertex<Object,Connect> **vert=NULL; // wskazniki na wezly grafu
  KolejkaPrio<Connect> Prio; // przechowuje krawedzie
  vertex<Object,Connect> **vertPod=NULL; // wskazniki na wezly drzewa rozpinajacego
  int *indeks=NULL; // do okreslenia ktory wierzcholek wskazywal na wybrany
  edge<Object,Connect> * Ed=NULL; // pomocniczy wskaznik krawedzi
  int liczba,dlFor,endOfV; // pomocnicze zmienne
  if(ilosc==0) return 0; // jezeli nie ma wierzcholkow w grafie
  D=new int[ilosc]; // tworzenie tablic o rozmiarze ilosci wierzcholkow
  vert=new vertex<Object,Connect>* [ilosc];
  vertPod=new vertex<Object,Connect>* [ilosc];
  indeks=new int[ilosc];
  srand(time(NULL)); // ustawienie zegara
  liczba=std::rand()%ilosc; // wylsowanie wierzcholka
  for(int i=0;i<ilosc;i++){ // przypisanie max do tablic
    D[i]=2147483647;
    indeks[i]=2147483647;
  }
  for(int i=0;i<ilosc;i++){ // przypisanie wskaznikow na wierzcholki grafu
    vert[i]=&graf[i];
  }
  if(graf.getTypGrafu()==0){ // jezeli graf jets na liscie sasiedztwa
    poddrzewo.insertVertex(vert[liczba]->getZawartosc()); // dodanie wylosowanego wierzcholka
    vertPod[liczba]=&poddrzewo.getListV().ostatni(); // przypisanie wskaznika na dodany element
    D[liczba]=0; // ustalenie odleglosci
    dlFor=vert[liczba]->getList().rozmiar(); // dla wszystkich krawedzi grafu
    for(int i=0;i<dlFor;i++){
      Ed=(*vert[liczba])[i]; // wskazanie na wybrana krawedz
      if(vert[liczba]==Ed->getV1())endOfV=graf.findint(*Ed->getV2()); // jezeli V1 to nasz wierzcholek to bierzemy adres z V2
      else endOfV=graf.findint(*Ed->getV1());
      indeks[endOfV]=liczba; // zapisanie który wierzcholek wskazywal na wybrany
      D[endOfV]=Ed->getZawartosc(); // zapisanie odleglosci
      Prio.dodaj(endOfV,D[endOfV]); // dodanie do kolejki priorytetowej
    }
    while(!Prio.isEmpty()){ // dopoki kolejka nie jest pusta
      liczba=Prio.usunMin().getZawartosc(); // usun min
      if(D[liczba]){ // jezeli D jest rozna od 0 czyli wierzcholek jeszcze nie zostal dodany
	poddrzewo.insertVertex(vert[liczba]->getZawartosc()); // dodajemy wierzcholek do drzewa rozpinajacego
	vertPod[liczba]=&poddrzewo.getListV().ostatni(); // przypisujemy mu wskaznik
	poddrzewo.insertEdge(*vertPod[liczba],*vertPod[indeks[liczba]],D[liczba]); // dokladamy krawedz laczaca nowy wierzcholek
	D[liczba]=0; // zaznaczmy ze juz jest dodany
	dlFor=vert[liczba]->getList().rozmiar(); // dla wszystkich krawedzi tego wierzcholka
	for(int i=0;i<dlFor;i++){
	  Ed=(*vert[liczba])[i]; // przypisujemy adres 
	  endOfV=graf.findint(*Ed->getV1()); // pobieramy dane o wskazywanym wiercholku
	  if(endOfV==liczba)endOfV=graf.findint(*Ed->getV2());
	  if(D[endOfV]!=0){ // jezeli jeszcze nie byl dodany
	    if(D[endOfV]==2147483647){ // gdy ani razu nie byl wskazany to go poprostu dodajmy
	      indeks[endOfV]=liczba; // zapisujemy jaki wierzcholek sie z nim laczy
	      D[endOfV]=Ed->getZawartosc(); // zapisujemy odleglosc miedzy nimi
	      Prio.dodaj(endOfV,D[endOfV]); // dodajemy do kolejki
	    }else{ // jezeli nie byl jeszcze do dany do struktury ale juz by lwskazany
	      if(Ed->getZawartosc()<D[endOfV]){ // patrzymy czy nowe wskazanie jest mniejsze
		Prio.usun(endOfV,D[endOfV]); // usuwamy z kolejki stare
		indeks[endOfV]=liczba; // zmieniamy indeks na nowy
		D[endOfV]=Ed->getZawartosc(); // zmieniamy odleglosc na nowa
		Prio.dodaj(endOfV,D[endOfV]); // dodajemy do kolejki nowe polaczenie
	      }
	    }
	  }
	}
      }
    }
  }else if(graf.getTypGrafu()==1){ // dla grafu na macierzy sasiedztwa
    poddrzewo.insertVertex(vert[liczba]->getZawartosc()); // tak samo jak na liscie
    vertPod[liczba]=&poddrzewo.getListV().ostatni();
    D[liczba]=0;
    dlFor=graf.getDlMatrix();
    for(int i=0;i<dlFor;i++){ // dla wszystkich mozliwych krawedzi
      Ed=&graf(liczba,i);
      if(Ed->getExist()){ // rozpatrujemy tylko te istniejace
	endOfV=i;
	indeks[endOfV]=liczba;
	D[endOfV]=Ed->getZawartosc();
	Prio.dodaj(endOfV,D[endOfV]);
      }
    }
    while(!Prio.isEmpty()){ // dopoki kolejka nie jest pusta
      liczba=Prio.usunMin().getZawartosc();
      if(D[liczba]){ // jezeli wierzcholek jeszcze nie jest dodany do drzewa
	poddrzewo.insertVertex(vert[liczba]->getZawartosc());
	vertPod[liczba]=&poddrzewo.getListV().ostatni();
	poddrzewo.insertEdge(*vertPod[liczba],*vertPod[indeks[liczba]],D[liczba]);
	D[liczba]=0;
	dlFor=graf.getDlMatrix();
	for(int i=0;i<dlFor;i++){ // dla wszystkich mozliwych krawedzi
	  Ed=&graf(liczba,i);
	  if(Ed->getExist()){ // rozpatrujemy tylko te istniejace
	    endOfV=i;
	    if(D[endOfV]!=0){
	      if(D[endOfV]==2147483647){
		indeks[endOfV]=liczba;
		D[endOfV]=Ed->getZawartosc();
		Prio.dodaj(endOfV,D[endOfV]);
	      }else{
		if(Ed->getZawartosc()<D[endOfV]){
		  Prio.usun(endOfV,D[endOfV]);
		  indeks[endOfV]=liczba;
		  D[endOfV]=Ed->getZawartosc();
		  Prio.dodaj(endOfV,D[endOfV]);
		}
	      }
	    }
	  }
	}
      }
    }    
  }
  Prio.clear(); // na wszelki wypadek czyscimy kolejke
  delete[](vert); // czyscimy wszystkie pomocncze tablice
  delete[](D);
  delete[](vertPod);
  delete[](indeks);
  return 1;
}
