#include<iostream>
#include <string>
#include <cstdlib>
#include <ctime>
#include "graf.hh"
#include "kolejka.hh"

using namespace std;

int dl_kolejki=0;

template <class Object,class Connect>
int Dijkstry(graf<Object,Connect>& poddrzewo,graf<Object,Connect>& graf);

int main(int argc, char *argv[]){
  graf<int,int> grafm(1);
  graf<int,int> graf2m(1);
  graf<int,int> graf2;
  graf<int,int> graf;
  graf.generate(10,100);
  cout<<endl<<"Krawedzie grafu";
  graf.edges();
  cout<<endl<<"Wierzcholki grafu"<<endl;
  graf.vertices();
  grafm.generate(10,100);
  cout<<endl<<"Krawedzie grafu na macierzy";
  graf.edges();
  cout<<endl<<"Wierzcholki grafu na macierzy"<<endl;
  graf.vertices();
  Dijkstry(graf2,graf);
  Dijkstry(graf2m,grafm);
  graf.clear();
  grafm.clear();
  cout<<endl<<endl<<"Krawedzie podgrafu";
  graf2.edges();
  cout<<endl<<"Wierzcholki podgrafu"<<endl;
  graf2.vertices();
  cout<<endl<<endl<<"Krawedzie podgrafu na macierzy"<<endl;
  graf2m.edges();
  cout<<endl<<"Wierzcholki podgrafu na macierzy"<<endl;
  graf2m.vertices();
  graf2.clear();
  graf2m.clear();
  cout<<endl<<endl;
}

template <class Object,class Connect>
int Dijkstry(graf<Object,Connect>& poddrzewo,graf<Object,Connect>& graf){
  int ilosc=graf.getListV().rozmiar(); // zmienna do pentli for
  int *D=NULL; // lista odleglosci do wierzcholkow od struktury juz polaczonych
  int *A=NULL; // lista odleglosci do wierzcholkow wierzcholka poczatkowego
  vertex<Object,Connect> **vert=NULL; // tablica wskaznikow na wierzcholki by nie przeszukiwac listy
  KolejkaPrio<Connect> Prio; // kolejka prio na dodawane krawedzie
  vertex<Object,Connect> **vertPod=NULL; // rownie tablica wskaznikow ale drzewa rozpinajacego
  int *indeks=NULL; // pamieta ktory wierzcholek wskazywal na ten o ktory pytamy
  edge<Object,Connect> * Ed=NULL; // wskaznik na pojedyncza krawedz by jej nie wywolywac kilka razy
  int liczba,dlFor,endOfV; // pomocnicze zmienne
  if(ilosc==0) return 0; // jezeli graf nie ma wierzcholkow to zakoncz 
  D=new int[ilosc]; // utworzenie tablic o rozmiarach liczby wierzcholkow
  A=new int[ilosc];
  vert=new vertex<Object,Connect>* [ilosc];
  vertPod=new vertex<Object,Connect>* [ilosc];
  indeks=new int[ilosc];
  srand(time(NULL)); // ustawianie zegara
  liczba=std::rand()%ilosc; // losowa liczba
  for(int i=0;i<ilosc;i++){ // przypisanie nieskonczonosci(max wartosci) do tablic int
    D[i]=2147483647;
    A[i]=2147483647;
    indeks[i]=2147483647;
  }
  for(int i=0;i<ilosc;i++){ // przypisanie wierzcholkow do wskaznikow
    vert[i]=&graf[i];
  }
  if(graf.getTypGrafu()==0){ // jezeli graf na liscie sasiedztwa
    poddrzewo.insertVertex(vert[liczba]->getZawartosc()); // dodaj wylosowany wierzcholek
    vertPod[liczba]=&poddrzewo.getListV().ostatni(); // przypisanie wskaznika
    D[liczba]=0; // ustalanie wartosci dla pierwszego wierzcholka
    A[liczba]=0;
    dlFor=vert[liczba]->getList().rozmiar(); // dla wrzystkich krawedzi jakie wychodza z tego wierzcholka
    for(int i=0;i<dlFor;i++){
      Ed=(*vert[liczba])[i]; // wskaznk na wybrana krawedz by nie przeszukiwac listy kilka razy
      endOfV=graf.findint(*Ed->getV1()); // pobranie adresu wierzcholka
      if(endOfV==liczba)endOfV=graf.findint(*Ed->getV2()); // jezeli adres wskazuje na nasz wierzcholek to wez drugi adres z krawedzi
      indeks[endOfV]=liczba; // przypisanie ktory wierzcholek wskazuje na nowy wierzcholek
      D[endOfV]=Ed->getZawartosc(); // przypisanie wartosci odleglosci wierzcholka
      A[endOfV]=Ed->getZawartosc();
      Prio.dodaj(endOfV,D[endOfV]); // dodanie do kolejki priorytetowej
    }
    while(!Prio.isEmpty()){ // dopoki w kolejce sa krawedzie
      liczba=Prio.usunMin().getZawartosc(); // usun min
      poddrzewo.insertVertex(vert[liczba]->getZawartosc()); // dodaj do drzewa rozpinajacego
      vertPod[liczba]=&poddrzewo.getListV().ostatni(); // przypisz wskaznik
      poddrzewo.insertEdge(*vertPod[liczba],*vertPod[indeks[liczba]],D[liczba]); // dodaj krawedz laczaca
      D[liczba]=0; // 0 bo teraz sa juz naleza do polaczonej klasy
      dlFor=vert[liczba]->getList().rozmiar(); // dla wszystkich krawedzi nowego wezla
      for(int i=0;i<dlFor;i++){
	Ed=(*vert[liczba])[i]; // powtorzenie jak przy pierwszym wezle
	endOfV=graf.findint(*Ed->getV1());
	if(endOfV==liczba)endOfV=graf.findint(*Ed->getV2());
	if(D[endOfV]!=0){ // jezeli wierzcholek wskazany jeszcze nie nalezy do polaczonych to dodaj krawedz
	  if(D[endOfV]==2147483647){ // jezeli max (jeszcze nie wskazany) to poprostu doadj do prio 
	    D[endOfV]=Ed->getZawartosc(); // dodaj odleglosc od struktury polaczonych
	    A[endOfV]=A[liczba]+Ed->getZawartosc(); // dodaj odelglosc od wierzcholka poczatkowego
	    indeks[endOfV]=liczba; // 
	    Prio.dodaj(endOfV,D[endOfV]);
	  }else{ // jezeli nie max ale wieksze od 0
	    A[endOfV]=A[liczba]+Ed->getZawartosc(); // zaktualizuj A
	    if(A[endOfV]<D[endOfV]){ // jezeli droga A jest krotsza od D to zamien D na A
	      Prio.usun(endOfV,D[endOfV]); // usuwanie starego polaczenia
	      indeks[endOfV]=liczba; // zmiana indeksu
	      D[endOfV]=Ed->getZawartosc(); // nadanie nowej odleglosci
	      Prio.dodaj(endOfV,D[endOfV]); // dodanie krawedzi
	    }
	  }
	}
      }
    }
  }else if(graf.getTypGrafu()==1){ // grafu na maciery sasiedztwa
    poddrzewo.insertVertex(vert[liczba]->getZawartosc()); // tak samo jak dla listy
    vertPod[liczba]=&poddrzewo.getListV().ostatni();
    D[liczba]=0;
    A[endOfV]=0;
    dlFor=graf.getDlMatrix();
    for(int i=0;i<dlFor;i++){ // dla wszystkich mozliwych krawedzi
      Ed=&graf(liczba,i); // przypisanie wskaznika
      if(Ed->getExist()){ // rozpatruj tylko te istniejace
	endOfV=i;
	indeks[endOfV]=liczba; // przypisanie indeksu wskazanemu wierzcholkowi
	D[endOfV]=Ed->getZawartosc(); // dodanie odleglosci wierzcholkowi
	A[endOfV]=Ed->getZawartosc();
	Prio.dodaj(endOfV,D[endOfV]); // dodanie do kolejki priorytetowej
      }
    }
    while(!Prio.isEmpty()){ // dopoki kolejka nie jest pusta
      liczba=Prio.usunMin().getZawartosc(); // powtorzenie jak przy liscie
      poddrzewo.insertVertex(vert[liczba]->getZawartosc());
      vertPod[liczba]=&poddrzewo.getListV().ostatni();
      poddrzewo.insertEdge(*vertPod[liczba],*vertPod[indeks[liczba]],D[liczba]);
      D[liczba]=0;
      dlFor=graf.getDlMatrix();
      for(int i=0;i<dlFor;i++){ // dla wszystkich mozliwych krawedzi
	Ed=&graf(liczba,i);
	if(Ed->getExist()){ // rozpatruj tylko istniejace
	  endOfV=i;
	  if(D[endOfV]!=0){
	    if(D[endOfV]==2147483647){
	      D[endOfV]=Ed->getZawartosc();
	      A[endOfV]=A[liczba]+Ed->getZawartosc();
	      Prio.dodaj(endOfV,D[endOfV]);
	      indeks[endOfV]=liczba;
	    }else{
	      A[endOfV]=A[liczba]+Ed->getZawartosc();
	      if(A[endOfV]<D[endOfV]){
		Prio.usun(endOfV,D[endOfV]);
		indeks[endOfV]=liczba;
		D[endOfV]=Ed->getZawartosc();
		Prio.dodaj(endOfV,D[endOfV]);
	      }
	    }
	  }
	}
      }
    }    
  }
  Prio.clear(); // czyszczeniekolekji i wszystkich tablic
  delete[](vert);
  delete[](D);
  delete[](A);
  delete[](vertPod);
  delete[](indeks);
  return 1; // 1 bo wykonano algorytm
}
