template <class Object>
class zawartoscKolejkiPrio{        // typ zmennej przechowywanej przez liste dwukierunkowa
protected:
  Object zawartosc;
  int priorytet;
public:
  zawartoscKolejkiPrio(){priorytet=0;}
  Object& getZawartosc(){return zawartosc;}
  Object& setZawartosc(const Object &dane){zawartosc=dane;return zawartosc;}
  int& getPrio(){return priorytet;}
  int& setPrio(const int &Priorytet){priorytet=Priorytet;return priorytet;}
};

template <class Object>
std::ostream& operator << (std::ostream& StrmWy,zawartoscKolejkiPrio<Object> &WyswietlanaWartosc){
  std::cout<<WyswietlanaWartosc.getZawartosc()<<" ";
  return StrmWy;  
}

template <class Object>
std::istream& operator >> (std::istream& StrmWy,zawartoscKolejkiPrio<Object> &dane){
  int wartosc=0;
  std::cin>>wartosc;  
  dane.setZawartosc(wartosc);
  return StrmWy;  
}

class KolejkaPrioEmptyException{};

template <class Object> // szablon kolejki priorytetowej
class KolejkaPrio: public doubleList<zawartoscKolejkiPrio<Object> >{  // dziedziczenie po liscie dwukierunkowej ktorej elementami sa zawartoscKolejkiPrio
public:
  zawartoscKolejkiPrio<Object>& dodaj(const int& obj,const int prio);  // f dodawania
  zawartoscKolejkiPrio<Object>& usunMin() throw(KolejkaPrioEmptyException);  // f usuwania minimalnej wartosci
  zawartoscKolejkiPrio<Object>& usun(const int& obj,const int prio) throw(KolejkaPrioEmptyException);  // f usuwania minimalnej wartosci
  zawartoscKolejkiPrio<Object>& min() throw(KolejkaPrioEmptyException);      // zwraca minimalna wartosc
  zawartoscKolejkiPrio<Object>& getHead(){return this->next->zawartosc();}   // zwraca wskaznk na pierwszy element
  KolejkaPrio(){this->dl=0;this->next=NULL;this->back=NULL;}
  bool isEmpty(){if((this->next==NULL))return true;return false;}
};


template <class Object>
zawartoscKolejkiPrio<Object>& KolejkaPrio<Object>::min() throw(KolejkaPrioEmptyException){
  doubleListInside<zawartoscKolejkiPrio<Object> > *tmp=this->next;
  doubleListInside<zawartoscKolejkiPrio<Object> > *min=this->next;
  if(this->next!=NULL){
    while(tmp!=NULL){ // dopoki sa elementy
      if((tmp->getZawartosc().getPrio())<(min->getZawartosc().getPrio())){
	min=tmp;
      }
      tmp=tmp->getNext();  // przejscie na nastepne elementy
    }
    this->zwrot=min->getZawartosc();
    return this->zwrot;
  }else{ // gdy kolejka pusta
    //cout<<"Kolejka jest pusta"<<endl;
    throw KolejkaPrioEmptyException(); // zrzucenie bledu 
  }
}

template <class Object>
zawartoscKolejkiPrio<Object>& KolejkaPrio<Object>::usunMin() throw(KolejkaPrioEmptyException){
  doubleListInside<zawartoscKolejkiPrio<Object> > *tmp=this->next;
  doubleListInside<zawartoscKolejkiPrio<Object> > *min=this->next;
  if(this->next!=NULL){
    while(tmp!=NULL){ // dopoki sa elementy
      if((tmp->getZawartosc().getPrio())<(min->getZawartosc().getPrio())){
	min=tmp;
      }
      tmp=tmp->getNext();  // przejscie na nastepne elementy
    }
    this->zwrot=min->getZawartosc();
    if(min->getBack()!=NULL){
      min->getBack()->getNext()=min->getNext();
    }else this->next=this->next->getNext();
    if(min->getNext()!=NULL){
      min->getNext()->getBack()=min->getBack();      
    }else this->back=this->back->getBack();
    delete(min); // zwolnienie pamieci
    this->dl--;
    if(this->next==NULL)this->back=NULL;
    return this->zwrot;
  }else{ // gdy kolejka pusta
    //cout<<"Kolejka jest pusta"<<endl;
    throw KolejkaPrioEmptyException(); // zrzucenie bledu 
  }
}

template <class Object>
zawartoscKolejkiPrio<Object>& KolejkaPrio<Object>::usun(const int& obj,const int prio)throw(KolejkaPrioEmptyException){
  doubleListInside<zawartoscKolejkiPrio<Object> > *tmp=this->next;
  while(tmp!=NULL){ // dopoki sa elementy
    if((tmp->getZawartosc().getPrio()==prio)&&(tmp->getZawartosc().getZawartosc()==obj)){
      this->zwrot=tmp->getZawartosc();
      if(tmp->getBack()!=NULL){
        tmp->getBack()->getNext()=tmp->getNext();
      }else this->next=this->next->getNext();
      if(tmp->getNext()!=NULL){
	tmp->getNext()->getBack()=tmp->getBack();      
      }else this->back=this->back->getBack();
      delete(tmp); // zwolnienie pamieci
      this->dl--;
      if(this->next==NULL)this->back=NULL;
      return this->zwrot;
    }
    tmp=tmp->getNext();  // przejscie na nastepne elementy
  }
  throw KolejkaPrioEmptyException(); // zrzucenie bledu 
}

template <class Object>
zawartoscKolejkiPrio<Object>& KolejkaPrio<Object>::dodaj(const int& obj,const int prio){
  doubleListInside<zawartoscKolejkiPrio<Object> > *tmp=new doubleListInside<zawartoscKolejkiPrio<Object> >;
  tmp->getZawartosc().setZawartosc(obj);
  tmp->getZawartosc().setPrio(prio);
  if(this->next==NULL){ // gdy kolejka pusta
    this->next=tmp;
  }else{          // gdy kolejka zawiera elementy
    this->back->setNext(tmp);
    tmp->getBack()=this->back;
  }
  this->back=tmp; // wprzypisanie pamieci do wskaznika koncowego
  this->dl++;
  return this->back->getZawartosc();
}
