#include<iostream>
#include <string>
#include <cstdlib>
#include <ctime>
#include "graf.hh"
#include "kolejka.hh"

using namespace std;

int dl_kolejki=0;

template <class Object,class Connect>
int Prima(graf<Object,Connect>& poddrzewo,graf<Object,Connect>& graf);

template <class Object,class Connect>
int Kruskala(graf<Object,Connect>& poddrzewo,graf<Object,Connect>& graf);

template <class Object,class Connect>
int Dijkstry(graf<Object,Connect>& poddrzewo,graf<Object,Connect>& graf);

int main(int argc, char *argv[]){
  char nazwa[12]="plikIWnr.tx";
  graf<int,int> poddrzewo;
  graf<int,int> poddrzewom(1);
  graf<int,int> grafm(1);
  graf<int,int> graf;
  ofstream plik_wyj("wyniki.txt");
  double czas;
  clock_t begin;              // zmienna od poczatku pomiaru
  clock_t end;                // zmienna od konca pomiaru
  int iloscElementow[]={10,50,100,500,250};
  int wypelnienie[]={25,50,75,100};
  nazwa[11]='t';
  if(argc==1){
    for(int i=0;i<5;i++){
      nazwa[4]='0'+i;
      for(int j=0;j<4;j++){
	nazwa[5]='0'+j;
	for(int k=0;k<100;k++){
	  nazwa[6]='0'+k/10;
	  nazwa[7]='0'+k%10;
	  graf.generateToFile(iloscElementow[i],wypelnienie[j],nazwa);
	  graf.clear();
	}
      }
    }
    cout<<"Wygenerowano pliki do pomiarow"<<endl;
    cout<<"By uruchomic pomiary uruchom program z parametrem p"<<endl;
    return 0;
  }
  for(int i=0;i<5;i++){
    nazwa[4]='0'+i;
    for(int j=0;j<4;j++){
      nazwa[5]='0'+j;
      plik_wyj<<"Czas Prima na liscie sasiedztwa. ";
      plik_wyj<<"Ilosc elementow: "<<iloscElementow[i];
      plik_wyj<<" wypelnienie: "<<wypelnienie[j]<<"%"<<endl;
      for(int k=0,licznik=1;k<100;k++){
	nazwa[6]='0'+k/10;
	nazwa[7]='0'+k%10;
	graf.load(nazwa);
	czas=0;
	begin=clock();  
	Prima(poddrzewo,graf);
	end=clock();
	czas=double(end - begin)/CLOCKS_PER_SEC;
	if(licznik<10)plik_wyj<<"  "<<licznik++;
	else if(licznik<100)plik_wyj<<" "<<licznik++;
	else plik_wyj<<licznik++;
	plik_wyj<<"  "<<czas<<endl;
	graf.clear();
	poddrzewo.clear();
      }
      plik_wyj<<"Czas Prima na macierzy sasiedztwa. ";
      plik_wyj<<"Ilosc elementow: "<<iloscElementow[i];
      plik_wyj<<" wypelnienie: "<<wypelnienie[j]<<"%"<<endl;
      for(int k=0,licznik=1;k<100;k++){
	nazwa[6]='0'+k/10;
	nazwa[7]='0'+k%10;
	grafm.load(nazwa);	
	czas=0;
	begin=clock();  
	Prima(poddrzewom,grafm);
	end=clock();
	czas=double(end - begin)/CLOCKS_PER_SEC;
	if(licznik<10)plik_wyj<<"  "<<licznik++;
	else if(licznik<100)plik_wyj<<" "<<licznik++;
	else plik_wyj<<licznik++;
	plik_wyj<<"  "<<czas<<endl;
	grafm.clear();
	poddrzewom.clear();
      }
      plik_wyj<<"Czas Kruskala na liscie sasiedztwa. ";
      plik_wyj<<"Ilosc elementow: "<<iloscElementow[i];
      plik_wyj<<" wypelnienie: "<<wypelnienie[j]<<"%"<<endl;
      for(int k=0,licznik=1;k<100;k++){
	nazwa[6]='0'+k/10;
	nazwa[7]='0'+k%10;
	graf.load(nazwa);
	czas=0;
	begin=clock();  
        Kruskala(poddrzewo,graf);
	end=clock();
	czas=double(end - begin)/CLOCKS_PER_SEC;
	if(licznik<10)plik_wyj<<"  "<<licznik++;
	else if(licznik<100)plik_wyj<<" "<<licznik++;
	else plik_wyj<<licznik++;
	plik_wyj<<"  "<<czas<<endl;
	graf.clear();
	poddrzewo.clear();	  
      }
      plik_wyj<<"Czas Kruskala na macierzy sasiedztwa. ";
      plik_wyj<<"Ilosc elementow: "<<iloscElementow[i];
      plik_wyj<<" wypelnienie: "<<wypelnienie[j]<<"%"<<endl;
      for(int k=0,licznik=1;k<100;k++){
	nazwa[6]='0'+k/10;
	nazwa[7]='0'+k%10;
	grafm.load(nazwa);	
	czas=0;
	begin=clock();  
	Kruskala(poddrzewom,grafm);
	end=clock();
	czas=double(end - begin)/CLOCKS_PER_SEC;
	if(licznik<10)plik_wyj<<"  "<<licznik++;
	else if(licznik<100)plik_wyj<<" "<<licznik++;
	else plik_wyj<<licznik++;
	plik_wyj<<"  "<<czas<<endl;
	grafm.clear();
	poddrzewom.clear();
      }
      plik_wyj<<"Czas Dijkstry na liscie sasiedztwa. ";
      plik_wyj<<"Ilosc elementow: "<<iloscElementow[i];
      plik_wyj<<" wypelnienie: "<<wypelnienie[j]<<"%"<<endl;
      for(int k=0,licznik=1;k<100;k++){
	nazwa[6]='0'+k/10;
	nazwa[7]='0'+k%10;
	graf.load(nazwa);
	czas=0;
	begin=clock();  
        Dijkstry(poddrzewo,graf);
	end=clock();
	czas=double(end - begin)/CLOCKS_PER_SEC;
	if(licznik<10)plik_wyj<<"  "<<licznik++;
	else if(licznik<100)plik_wyj<<" "<<licznik++;
	else plik_wyj<<licznik++;
	plik_wyj<<"  "<<czas<<endl;
	graf.clear();
	poddrzewo.clear();	  
      }
      plik_wyj<<"Czas Dijkstry na macierzy sasiedztwa. ";
      plik_wyj<<"Ilosc elementow: "<<iloscElementow[i];
      plik_wyj<<" wypelnienie: "<<wypelnienie[j]<<"%"<<endl;
      for(int k=0,licznik=1;k<100;k++){
	nazwa[6]='0'+k/10;
	nazwa[7]='0'+k%10;
	grafm.load(nazwa);	
	czas=0;
	begin=clock();  
        Dijkstry(poddrzewom,grafm);
	end=clock();
	czas=double(end - begin)/CLOCKS_PER_SEC;
	if(licznik<10)plik_wyj<<"  "<<licznik++;
	else if(licznik<100)plik_wyj<<" "<<licznik++;
	else plik_wyj<<licznik++;
	plik_wyj<<"  "<<czas<<endl;
	grafm.clear();
	poddrzewom.clear();
      }
    }
  }
  plik_wyj.close(); // zamkniecie pliku  
}

template <class Object,class Connect>
int Dijkstry(graf<Object,Connect>& poddrzewo,graf<Object,Connect>& graf){
  int ilosc=graf.getListV().rozmiar(); // zmienna do pentli for
  int *D=NULL; // lista odleglosci do wierzcholkow od struktury juz polaczonych
  int *A=NULL; // lista odleglosci do wierzcholkow wierzcholka poczatkowego
  vertex<Object,Connect> **vert=NULL; // tablica wskaznikow na wierzcholki by nie przeszukiwac listy
  KolejkaPrio<Connect> Prio; // kolejka prio na dodawane krawedzie
  vertex<Object,Connect> **vertPod=NULL; // rownie tablica wskaznikow ale drzewa rozpinajacego
  int *indeks=NULL; // pamieta ktory wierzcholek wskazywal na ten o ktory pytamy
  edge<Object,Connect> * Ed=NULL; // wskaznik na pojedyncza krawedz by jej nie wywolywac kilka razy
  int liczba,dlFor,endOfV; // pomocnicze zmienne
  if(ilosc==0) return 0; // jezeli graf nie ma wierzcholkow to zakoncz 
  D=new int[ilosc]; // utworzenie tablic o rozmiarach liczby wierzcholkow
  A=new int[ilosc];
  vert=new vertex<Object,Connect>* [ilosc];
  vertPod=new vertex<Object,Connect>* [ilosc];
  indeks=new int[ilosc];
  srand(time(NULL)); // ustawianie zegara
  liczba=std::rand()%ilosc; // losowa liczba
  for(int i=0;i<ilosc;i++){ // przypisanie nieskonczonosci(max wartosci) do tablic int
    D[i]=2147483647;
    A[i]=2147483647;
    indeks[i]=2147483647;
  }
  for(int i=0;i<ilosc;i++){ // przypisanie wierzcholkow do wskaznikow
    vert[i]=&graf[i];
  }
  if(graf.getTypGrafu()==0){ // jezeli graf na liscie sasiedztwa
    poddrzewo.insertVertex(vert[liczba]->getZawartosc()); // dodaj wylosowany wierzcholek
    vertPod[liczba]=&poddrzewo.getListV().ostatni(); // przypisanie wskaznika
    D[liczba]=0; // ustalanie wartosci dla pierwszego wierzcholka
    A[liczba]=0;
    dlFor=vert[liczba]->getList().rozmiar(); // dla wrzystkich krawedzi jakie wychodza z tego wierzcholka
    for(int i=0;i<dlFor;i++){
      Ed=(*vert[liczba])[i]; // wskaznk na wybrana krawedz by nie przeszukiwac listy kilka razy
      endOfV=graf.findint(*Ed->getV1()); // pobranie adresu wierzcholka
      if(endOfV==liczba)endOfV=graf.findint(*Ed->getV2()); // jezeli adres wskazuje na nasz wierzcholek to wez drugi adres z krawedzi
      indeks[endOfV]=liczba; // przypisanie ktory wierzcholek wskazuje na nowy wierzcholek
      D[endOfV]=Ed->getZawartosc(); // przypisanie wartosci odleglosci wierzcholka
      A[endOfV]=Ed->getZawartosc();
      Prio.dodaj(endOfV,D[endOfV]); // dodanie do kolejki priorytetowej
    }
    while(!Prio.isEmpty()){ // dopoki w kolejce sa krawedzie
      liczba=Prio.usunMin().getZawartosc(); // usun min
      poddrzewo.insertVertex(vert[liczba]->getZawartosc()); // dodaj do drzewa rozpinajacego
      vertPod[liczba]=&poddrzewo.getListV().ostatni(); // przypisz wskaznik
      poddrzewo.insertEdge(*vertPod[liczba],*vertPod[indeks[liczba]],D[liczba]); // dodaj krawedz laczaca
      D[liczba]=0; // 0 bo teraz sa juz naleza do polaczonej klasy
      dlFor=vert[liczba]->getList().rozmiar(); // dla wszystkich krawedzi nowego wezla
      for(int i=0;i<dlFor;i++){
	Ed=(*vert[liczba])[i]; // powtorzenie jak przy pierwszym wezle
	endOfV=graf.findint(*Ed->getV1());
	if(endOfV==liczba)endOfV=graf.findint(*Ed->getV2());
	if(D[endOfV]!=0){ // jezeli wierzcholek wskazany jeszcze nie nalezy do polaczonych to dodaj krawedz
	  if(D[endOfV]==2147483647){ // jezeli max (jeszcze nie wskazany) to poprostu doadj do prio 
	    D[endOfV]=Ed->getZawartosc(); // dodaj odleglosc od struktury polaczonych
	    A[endOfV]=A[liczba]+Ed->getZawartosc(); // dodaj odelglosc od wierzcholka poczatkowego
	    indeks[endOfV]=liczba; // 
	    Prio.dodaj(endOfV,D[endOfV]);
	  }else{ // jezeli nie max ale wieksze od 0
	    A[endOfV]=A[liczba]+Ed->getZawartosc(); // zaktualizuj A
	    if(A[endOfV]<D[endOfV]){ // jezeli droga A jest krotsza od D to zamien D na A
	      Prio.usun(endOfV,D[endOfV]); // usuwanie starego polaczenia
	      indeks[endOfV]=liczba; // zmiana indeksu
	      D[endOfV]=Ed->getZawartosc(); // nadanie nowej odleglosci
	      Prio.dodaj(endOfV,D[endOfV]); // dodanie krawedzi
	    }
	  }
	}
      }
    }
  }else if(graf.getTypGrafu()==1){ // grafu na maciery sasiedztwa
    poddrzewo.insertVertex(vert[liczba]->getZawartosc()); // tak samo jak dla listy
    vertPod[liczba]=&poddrzewo.getListV().ostatni();
    D[liczba]=0;
    A[endOfV]=0;
    dlFor=graf.getDlMatrix();
    for(int i=0;i<dlFor;i++){ // dla wszystkich mozliwych krawedzi
      Ed=&graf(liczba,i); // przypisanie wskaznika
      if(Ed->getExist()){ // rozpatruj tylko te istniejace
	endOfV=i;
	indeks[endOfV]=liczba; // przypisanie indeksu wskazanemu wierzcholkowi
	D[endOfV]=Ed->getZawartosc(); // dodanie odleglosci wierzcholkowi
	A[endOfV]=Ed->getZawartosc();
	Prio.dodaj(endOfV,D[endOfV]); // dodanie do kolejki priorytetowej
      }
    }
    while(!Prio.isEmpty()){ // dopoki kolejka nie jest pusta
      liczba=Prio.usunMin().getZawartosc(); // powtorzenie jak przy liscie
      poddrzewo.insertVertex(vert[liczba]->getZawartosc());
      vertPod[liczba]=&poddrzewo.getListV().ostatni();
      poddrzewo.insertEdge(*vertPod[liczba],*vertPod[indeks[liczba]],D[liczba]);
      D[liczba]=0;
      dlFor=graf.getDlMatrix();
      for(int i=0;i<dlFor;i++){ // dla wszystkich mozliwych krawedzi
	Ed=&graf(liczba,i);
	if(Ed->getExist()){ // rozpatruj tylko istniejace
	  endOfV=i;
	  if(D[endOfV]!=0){
	    if(D[endOfV]==2147483647){
	      D[endOfV]=Ed->getZawartosc();
	      A[endOfV]=A[liczba]+Ed->getZawartosc();
	      Prio.dodaj(endOfV,D[endOfV]);
	      indeks[endOfV]=liczba;
	    }else{
	      A[endOfV]=A[liczba]+Ed->getZawartosc();
	      if(A[endOfV]<D[endOfV]){
		Prio.usun(endOfV,D[endOfV]);
		indeks[endOfV]=liczba;
		D[endOfV]=Ed->getZawartosc();
		Prio.dodaj(endOfV,D[endOfV]);
	      }
	    }
	  }
	}
      }
    }    
  }
  Prio.clear(); // czyszczeniekolekji i wszystkich tablic
  delete[](vert);
  delete[](D);
  delete[](A);
  delete[](vertPod);
  delete[](indeks);
  return 1; // 1 bo wykonano algorytm
}

template <class Object,class Connect>
int Prima(graf<Object,Connect>& poddrzewo,graf<Object,Connect>& graf){
  int ilosc=graf.getListV().rozmiar(); // okresla ilosc wierzcholkow
  int *D=NULL; // do okreslenia odleglosci wierzcholkow od polaczonej struktory
  vertex<Object,Connect> **vert=NULL; // wskazniki na wezly grafu
  KolejkaPrio<Connect> Prio; // przechowuje krawedzie
  vertex<Object,Connect> **vertPod=NULL; // wskazniki na wezly drzewa rozpinajacego
  int *indeks=NULL; // do okreslenia ktory wierzcholek wskazywal na wybrany
  edge<Object,Connect> * Ed=NULL; // pomocniczy wskaznik krawedzi
  int liczba,dlFor,endOfV; // pomocnicze zmienne
  if(ilosc==0) return 0; // jezeli nie ma wierzcholkow w grafie
  D=new int[ilosc]; // tworzenie tablic o rozmiarze ilosci wierzcholkow
  vert=new vertex<Object,Connect>* [ilosc];
  vertPod=new vertex<Object,Connect>* [ilosc];
  indeks=new int[ilosc];
  srand(time(NULL)); // ustawienie zegara
  liczba=std::rand()%ilosc; // wylsowanie wierzcholka
  for(int i=0;i<ilosc;i++){ // przypisanie max do tablic
    D[i]=2147483647;
    indeks[i]=2147483647;
  }
  for(int i=0;i<ilosc;i++){ // przypisanie wskaznikow na wierzcholki grafu
    vert[i]=&graf[i];
  }
  if(graf.getTypGrafu()==0){ // jezeli graf jets na liscie sasiedztwa
    poddrzewo.insertVertex(vert[liczba]->getZawartosc()); // dodanie wylosowanego wierzcholka
    vertPod[liczba]=&poddrzewo.getListV().ostatni(); // przypisanie wskaznika na dodany element
    D[liczba]=0; // ustalenie odleglosci
    dlFor=vert[liczba]->getList().rozmiar(); // dla wszystkich krawedzi grafu
    for(int i=0;i<dlFor;i++){
      Ed=(*vert[liczba])[i]; // wskazanie na wybrana krawedz
      if(vert[liczba]==Ed->getV1())endOfV=graf.findint(*Ed->getV2()); // jezeli V1 to nasz wierzcholek to bierzemy adres z V2
      else endOfV=graf.findint(*Ed->getV1());
      indeks[endOfV]=liczba; // zapisanie kt�ry wierzcholek wskazywal na wybrany
      D[endOfV]=Ed->getZawartosc(); // zapisanie odleglosci
      Prio.dodaj(endOfV,D[endOfV]); // dodanie do kolejki priorytetowej
    }
    while(!Prio.isEmpty()){ // dopoki kolejka nie jest pusta
      liczba=Prio.usunMin().getZawartosc(); // usun min
      if(D[liczba]){ // jezeli D jest rozna od 0 czyli wierzcholek jeszcze nie zostal dodany
	poddrzewo.insertVertex(vert[liczba]->getZawartosc()); // dodajemy wierzcholek do drzewa rozpinajacego
	vertPod[liczba]=&poddrzewo.getListV().ostatni(); // przypisujemy mu wskaznik
	poddrzewo.insertEdge(*vertPod[liczba],*vertPod[indeks[liczba]],D[liczba]); // dokladamy krawedz laczaca nowy wierzcholek
	D[liczba]=0; // zaznaczmy ze juz jest dodany
	dlFor=vert[liczba]->getList().rozmiar(); // dla wszystkich krawedzi tego wierzcholka
	for(int i=0;i<dlFor;i++){
	  Ed=(*vert[liczba])[i]; // przypisujemy adres 
	  endOfV=graf.findint(*Ed->getV1()); // pobieramy dane o wskazywanym wiercholku
	  if(endOfV==liczba)endOfV=graf.findint(*Ed->getV2());
	  if(D[endOfV]!=0){ // jezeli jeszcze nie byl dodany
	    if(D[endOfV]==2147483647){ // gdy ani razu nie byl wskazany to go poprostu dodajmy
	      indeks[endOfV]=liczba; // zapisujemy jaki wierzcholek sie z nim laczy
	      D[endOfV]=Ed->getZawartosc(); // zapisujemy odleglosc miedzy nimi
	      Prio.dodaj(endOfV,D[endOfV]); // dodajemy do kolejki
	    }else{ // jezeli nie byl jeszcze do dany do struktury ale juz by lwskazany
	      if(Ed->getZawartosc()<D[endOfV]){ // patrzymy czy nowe wskazanie jest mniejsze
		Prio.usun(endOfV,D[endOfV]); // usuwamy z kolejki stare
		indeks[endOfV]=liczba; // zmieniamy indeks na nowy
		D[endOfV]=Ed->getZawartosc(); // zmieniamy odleglosc na nowa
		Prio.dodaj(endOfV,D[endOfV]); // dodajemy do kolejki nowe polaczenie
	      }
	    }
	  }
	}
      }
    }
  }else if(graf.getTypGrafu()==1){ // dla grafu na macierzy sasiedztwa
    poddrzewo.insertVertex(vert[liczba]->getZawartosc()); // tak samo jak na liscie
    vertPod[liczba]=&poddrzewo.getListV().ostatni();
    D[liczba]=0;
    dlFor=graf.getDlMatrix();
    for(int i=0;i<dlFor;i++){ // dla wszystkich mozliwych krawedzi
      Ed=&graf(liczba,i);
      if(Ed->getExist()){ // rozpatrujemy tylko te istniejace
	endOfV=i;
	indeks[endOfV]=liczba;
	D[endOfV]=Ed->getZawartosc();
	Prio.dodaj(endOfV,D[endOfV]);
      }
    }
    while(!Prio.isEmpty()){ // dopoki kolejka nie jest pusta
      liczba=Prio.usunMin().getZawartosc();
      if(D[liczba]){ // jezeli wierzcholek jeszcze nie jest dodany do drzewa
	poddrzewo.insertVertex(vert[liczba]->getZawartosc());
	vertPod[liczba]=&poddrzewo.getListV().ostatni();
	poddrzewo.insertEdge(*vertPod[liczba],*vertPod[indeks[liczba]],D[liczba]);
	D[liczba]=0;
	dlFor=graf.getDlMatrix();
	for(int i=0;i<dlFor;i++){ // dla wszystkich mozliwych krawedzi
	  Ed=&graf(liczba,i);
	  if(Ed->getExist()){ // rozpatrujemy tylko te istniejace
	    endOfV=i;
	    if(D[endOfV]!=0){
	      if(D[endOfV]==2147483647){
		indeks[endOfV]=liczba;
		D[endOfV]=Ed->getZawartosc();
		Prio.dodaj(endOfV,D[endOfV]);
	      }else{
		if(Ed->getZawartosc()<D[endOfV]){
		  Prio.usun(endOfV,D[endOfV]);
		  indeks[endOfV]=liczba;
		  D[endOfV]=Ed->getZawartosc();
		  Prio.dodaj(endOfV,D[endOfV]);
		}
	      }
	    }
	  }
	}
      }
    }    
  }
  Prio.clear(); // na wszelki wypadek czyscimy kolejke
  delete[](vert); // czyscimy wszystkie pomocncze tablice
  delete[](D);
  delete[](vertPod);
  delete[](indeks);
  return 1;
}

template <class Object,class Connect>
int Kruskala(graf<Object,Connect>& poddrzewo,graf<Object,Connect>& graf){
  int iloscE=graf.getListE().rozmiar(); // okresla ilosc krawedzi w grafie
  int iloscV=graf.getListV().rozmiar(); // okresla ilosc wierzcholkow w grafie
  vertex<Object,Connect> **vert=new vertex<Object,Connect>* [iloscV]; // tablica wskaznikow na wierzcholki grafu
  vertex<Object,Connect> **vertPod=new vertex<Object,Connect>* [iloscV]; // tablica wskaznikow na wierzcholki drzewa rozpinajacego
  int *klaster=new int[iloscV]; // tablica okreslajaca czy wierzcholki sa polaczone
  bool *istnieje=new bool[iloscV]; // tablica okreslajaca czy wybrane wierzcholki ju� zostaly doadane do poddrzewa
  edge<Object,Connect>* Ed=NULL; // pomocniczy wskaznik na krawedz
  KolejkaPrio<edge<Object,Connect>* > Prio; // kolejka priorytetowa
  int V1,V2; //pomocnicze zmienne
  if(graf.getListV().rozmiar()==0) return 0; // jezeli nie ma wierzcholkow to zakoncz
  else if(graf.getListV().rozmiar()==1){ // jezeli jest tylko jeden wierzcholek to min drzewo rozpinajace to on
    poddrzewo.insertVertex(graf[1].getZawartosc());
    return 0;
  }
  for(int i=0;i<iloscV;i++){ // dodanie adresow na wierzcholki i rzygotowanie tablic
    vert[i]=&graf[i];
    klaster[i]=i;
    istnieje[i]=false;
  }
  if(graf.getTypGrafu()==0){ // jezeli graf na liscie sasiedztwa
    for(int i=0;i<iloscE;i++){ // dodanie adresow krawedzie do tablicy
      Ed=&graf(i);
      Prio.dodaj(Ed,Ed->getZawartosc()); // wsadzenie wszystkich krawedzi do kolejki priorytetowej
    }
    while(!Prio.isEmpty()){ // dopoki kolejka nie jest pusta
      Ed=Prio.usunMin().getZawartosc(); // dodanie adresu zdjetej krawedzi to pomocniczego wskaznika
      V1=graf.findint(*Ed->getV1()); // podanie indeksu jednego wierzcholka krawedzi
      V2=graf.findint(*Ed->getV2()); // podanie indeksu drugiego wierzcholka krawedzi
      if(klaster[V1]!=klaster[V2]){ // jezeli jeszcze nie sa polaczone
	if(!istnieje[V1]){ // jezeli jeszcze nie ma tego wierzcholka w drzewie rozpinajacym
	  istnieje[V1]=true; // zaznaccza ze juz jest w drzewie
	  poddrzewo.insertVertex(vert[V1]->getZawartosc()); // dodaje do drzewa
	  vertPod[V1]=&poddrzewo.getListV().ostatni(); // dodaje adres wierzcholka do tablicy
	}
	if(!istnieje[V2]){ // to samo dla drugiego wiercholka krawedzi
	  istnieje[V2]=true;
	  poddrzewo.insertVertex(vert[V2]->getZawartosc());
	  vertPod[V2]=&poddrzewo.getListV().ostatni();
	}
	poddrzewo.insertEdge(*vertPod[V1],*vertPod[V2],Ed->getZawartosc()); // laczy wybrane wierzcholki
	for(int i=0,j=klaster[V2];i<iloscV;i++)if(klaster[i]==j)klaster[i]=klaster[V1]; // wszystkim polaczonym wierzcholka nadaje jeden klaster
      }
    }
  }else if(graf.getTypGrafu()==1){ // dla grafu na macierzy sasiedztwa
    for(int i=0;i<iloscV;i++){
      for(int j=0;j<i;j++){ // dla wszystkich mozliwych wierzcholkow
	Ed=&graf(i,j); // pomocniczy wskaznik na wybrana krawedz
	if(Ed->getExist()){ // rozpatrz tylko istniejace
	  Prio.dodaj(Ed,Ed->getZawartosc()); // dodaj do kolejki priorytetowej
	}
      }
    }
    while(!Prio.isEmpty()){ // analogicznie jak dla listy sasiedztwa
      Ed=Prio.usunMin().getZawartosc();
      V1=graf.findint(graf.endVerticies(*Ed,0));
      V2=graf.findint(graf.endVerticies(*Ed,1));
      if(klaster[V1]!=klaster[V2]){
	if(!istnieje[V1]){
	  istnieje[V1]=true;
	  poddrzewo.insertVertex(vert[V1]->getZawartosc());
	  vertPod[V1]=&poddrzewo.getListV().ostatni();
	}
	if(!istnieje[V2]){
	  istnieje[V2]=true;
	  poddrzewo.insertVertex(vert[V2]->getZawartosc());
	  vertPod[V2]=&poddrzewo.getListV().ostatni();
	}
	poddrzewo.insertEdge(*vertPod[V1],*vertPod[V2],Ed->getZawartosc());
	for(int i=0,j=klaster[V2];i<iloscV;i++)if(klaster[i]==j)klaster[i]=klaster[V1];
      }
    }
  }
  Prio.clear(); // czyszczenie kolejki dla pewnosci
  delete[](vert); // czyszczenie wszystkich pomocniczych tablic
  delete[](vertPod);
  delete[](klaster);
  delete[](istnieje);
  return 1;
}
